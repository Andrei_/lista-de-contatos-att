package poo.estrutura.base.tui;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import javax.swing.JOptionPane;
import javax.swing.JTextField;

import poo.estrutura.base.business.ContatoBO;
import poo.estrutura.base.domain.Contato;
import poo.estrutura.base.enums.TipoContatoEnum;

public class ContatoTui {
	private ContatoBO contatoBO = new ContatoBO();
	private Scanner sc = new Scanner(System.in);
	public SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

	public void include() {
		System.out.println("INCLUS�O DE CONTATOS\n");
		getContato();

	}

	public void listar() {
		System.out.println("LISTAGEM DE CONTATOS\n");
		listagem();
	}

	public void excluir() {
		System.out.println("REMO��O DE CONTATO\n");
		retirar();
	}

	public void pesquisar() {
		System.out.println("PESQUISA DE CONTATOS");
		find();
	}

	private Date getData(String txt) {

		while (true) {
			try {
				System.out.println(txt);
				String dataString = sc.nextLine();
				Date data = sdf.parse(dataString);
				return data;
			} catch (Exception e) {
				System.out.println("Informe a data novamente: ");
			}
		}

	}

	private String getString(String txt) {
		while (true) {
			System.out.println(txt);
			String texto = sc.nextLine();
			try {
				contatoBO.validarTextoObrigatorio(texto, txt);
				return texto;
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
	}

	private Integer getInteiro(String txt) {

		System.out.println(txt);
		while (true) {
			try {
				Integer numero = sc.nextInt();
				sc.nextLine();
				return numero;

			} catch (Exception e) {
				sc.nextLine();
				System.out.println("Por favor informe um n�mero");
			}
		}
	}

	private TipoContatoEnum getTipo(String txt) {
		while (true) {
			System.out.println(txt);
			String tipoString = sc.nextLine();
			try {
				TipoContatoEnum tipoContato = TipoContatoEnum.valueOf(tipoString.toUpperCase());
				return tipoContato;
			} catch (IllegalArgumentException e) {
				System.out.println("Tipo informado n�o v�lido.");
			}
		}
	}

	private void getContato() {

		getContato2();
		String nome = getString("Informe o nome: ");
		String telefone = getString("Informe o n�mero: ");
		Integer anoNasc = getInteiro("Informe o ano de nascimento: ");
		Date dataNascimento = getData("Informe a data de nascimento(dia/m�s/ano): ");
		TipoContatoEnum tipo = getTipo("Informe o seu estado (" + getTipo() + ")");
		Contato contato = new Contato(nome, telefone, anoNasc, dataNascimento, tipo);

		try {
			contatoBO.include(contato);
		} catch (Exception err) {
			System.out.println(err.getMessage());
		}

	}

	private void getContato2() {

		JTextField field1 = new JTextField();
		JTextField field2 = new JTextField();
		JTextField field3 = new JTextField();
		JTextField field4 = new JTextField();
		JTextField field5 = new JTextField();
		String nome = field1.getText();
		String telefone = field2.getText();
		Integer anoNasc = Integer.parseInt(field3.getText());
		Date dataNasc;
		try {
			dataNasc = sdf.parse(field4.getText());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		TipoContatoEnum estado = TipoContatoEnum.valueOf(field5.getText());
		Object[] fields = { "Nome", nome, "Telefone", telefone, "Ano de nascimento", anoNasc, "Data de nascimento",
				dataNasc, "Estado", estado };
		JOptionPane.showConfirmDialog(null, fields, "Informe os dados do contato", JOptionPane.OK_CANCEL_OPTION,
				JOptionPane.INFORMATION_MESSAGE, null);
		Contato contato = new Contato(nome, telefone, anoNasc, dataNasc, estado);

		try {
			contatoBO.include(contato);
		} catch (Exception err) {
			System.out.println(err.getMessage());
		}
	}

	private void listagem() {
		List<Contato> contatos = contatoBO.getAll();

		mostrar(contatos);
	}

	private void mostrar(List<Contato> contatos) {
		for (Contato contato : contatos) {
			Object[] fields = { "Nome", contato.getNome(), "Telefone", contato.getTelefone(), "Ano de nascimento",
					contato.getAnoNasc(), "Data de nascimento", sdf.format(contato.getDataNascimento()), "Estado",
					contato.getTipo() };
			JOptionPane.showMessageDialog(null, fields, "Lista de contatos", JOptionPane.OK_CANCEL_OPTION);
		}
	}

	private void retirar() {
		List<Contato> contatos = contatoBO.getAll();
		Integer opc = 0;
		String txt = getString("Informe o contato que deseja remover");
		Contato contato = contatoBO.findByNome(txt);
		if (contato == null) {
			JOptionPane.showMessageDialog(null, "Este contato n�o existe!!!");
		} else {
			opc = JOptionPane.showConfirmDialog(null, "Deseja realmente remover este contato ? " + contato.getNome(),
					"Aviso", JOptionPane.YES_NO_OPTION);
			if (opc == JOptionPane.YES_OPTION) {
				contatos.remove(contato);
			}
		}
	}

	private void retirarOriginal() {
		List<Contato> contatos = contatoBO.getAll();
		Contato contatoRemover = null;
		Integer opc = 0;
		String txt = getString("Informe o contato que deseja remover");
		for (Contato contato : contatos) {
			if (contato.getNome().equalsIgnoreCase(txt)) {
				opc = JOptionPane.showConfirmDialog(null,
						"Deseja realmente remover este contato ? " + contato.getNome(), "Aviso",
						JOptionPane.YES_NO_OPTION);
				if (opc == JOptionPane.YES_OPTION) {
					contatoRemover = contato;

				} else if (opc == JOptionPane.NO_OPTION) {
					break;
				}

			} else {
				JOptionPane.showMessageDialog(null, "Este contato n�o existe!!!");
			}
		}
		if (contatoRemover != null) {
			contatos.remove(contatoRemover);
		}
	}

	private String getTipo() {
		int idx = 0;
		int length = TipoContatoEnum.values().length;
		String itens = "";
		for (TipoContatoEnum tipo : TipoContatoEnum.values()) {
			itens += (tipo + (idx == (length - 1) ? "" : ", "));
			idx++;
		}
		return itens.substring(0, itens.length() - 1);
	}

	private void find() {
		List<Contato> contatos = contatoBO.getAll();
		String nome = getString("Informe o nome do contato que deseja pesquisar");
		String telefone = getString("Informe o telefone do contato que deseja pesquisar");
		Contato contato = contatoBO.find(nome, telefone);
		mostrar(contatos);
	}
}