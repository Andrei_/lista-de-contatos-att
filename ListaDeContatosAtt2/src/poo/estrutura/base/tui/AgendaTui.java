package poo.estrutura.base.tui;

import java.util.Scanner;

import javax.swing.JOptionPane;

public class AgendaTui {

	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		ContatoTui contatoTui = new ContatoTui();
		Integer opc = 0;
		opc = menu();

		while (opc != 4) {
			if (opc == 0) {
				opc = incluir(contatoTui, opc);
			}
			if (opc == 1) {
				contatoTui.listar();
				opc = escolher(opc);
			}
			if (opc == 2) {
				opc = excluir(contatoTui, opc);
			}
			if (opc == 3) {
                contatoTui.pesquisar();
                opc = escolher(opc);
			}
		}
	}

	private static Integer excluir(ContatoTui contatoTui, Integer opc) {
		Integer opc2;
		contatoTui.excluir();
		
		opc2 = JOptionPane.showConfirmDialog(null, "Deseja continuar removendo contatos ? ", "Aviso",
				JOptionPane.YES_NO_OPTION);
		while (opc2 == JOptionPane.YES_NO_OPTION) {
			contatoTui.excluir();
			opc2 = JOptionPane.showConfirmDialog(null, "Deseja continuar removendo contatos ? ", "Aviso",
					JOptionPane.YES_NO_OPTION);
		}
		if (opc2 == JOptionPane.NO_OPTION) {
			opc = escolher(opc);
		}
		return opc;
	}

	private static Integer incluir(ContatoTui contatoTui, Integer opc) {
		Integer opc2;
		contatoTui.include();

		opc2 = JOptionPane.showConfirmDialog(null, "Deseja continuar adicionando contatos ? ", "Aviso",
				JOptionPane.YES_NO_OPTION);

		while (opc2 == 0) {
			contatoTui.include();
			opc2 = JOptionPane.showConfirmDialog(null, "Deseja continuar adicionando contatos ? ", "Aviso",
					JOptionPane.YES_NO_OPTION);
		}

		if (opc2 == JOptionPane.NO_OPTION) {
			opc = escolher(opc);
		}
		return opc;
	}

	private static Integer escolher(Integer opc) {
		Integer opc2;
		opc2 = JOptionPane.showConfirmDialog(null, "Deseja voltar para o menu ?", "Aviso", JOptionPane.YES_NO_OPTION);

		if (opc2 == JOptionPane.YES_OPTION) {
			opc = menu();
		} else if (opc2 == JOptionPane.NO_OPTION) {
			JOptionPane.showMessageDialog(null, "Agenda finalizada");
			opc = 4;
		}
		return opc;
	}

	private static Integer menu() {
		Integer opc = 0;
		String[] menu = { "Incluir", "Listar", "Excluir", "Pesquisar", "Finalizar" };
		opc = JOptionPane.showOptionDialog(null, "O que deseja ?", "Menu", JOptionPane.DEFAULT_OPTION,
				JOptionPane.INFORMATION_MESSAGE, null, menu, menu[0]);
		return opc;
	}

}
