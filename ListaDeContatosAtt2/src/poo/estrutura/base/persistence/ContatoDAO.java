package poo.estrutura.base.persistence;

import java.util.ArrayList;
import java.util.List;

import poo.estrutura.base.domain.Contato;

public class ContatoDAO {

	private List<Contato> contatos = new ArrayList<>();

	public String include(Contato contato) {
		contatos.add(contato);
		return null;
	}

	public List<Contato> getAll() {
		return contatos;
	}

	public Contato findByNome(String txt) {
		for (Contato contato : contatos) {
			if (contato.getNome().equalsIgnoreCase(txt)) {
				return contato;
			}
		}
		return null;
	}

	public Contato find(String nome, String telefone) {
		for (Contato contato : contatos) {
			if (contato.getNome().equalsIgnoreCase(nome) && contato.getTelefone().equalsIgnoreCase(telefone)) {
				return contato;
			}
		}
		return null;
	}
}
