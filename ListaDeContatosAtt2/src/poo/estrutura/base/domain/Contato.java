package poo.estrutura.base.domain;

import java.util.Date;

import javax.xml.crypto.Data;

import poo.estrutura.base.enums.TipoContatoEnum;

/*Inserir o exception para tratar com erros, estudar List,  */
public class Contato {

	private String nome;
	private String telefone;
	private Integer anoNasc;
	private Date dataNascimento;
	private TipoContatoEnum tipo;

	public TipoContatoEnum getTipo() {
		return tipo;
	}

	public void setTipo(TipoContatoEnum tipo) {
		this.tipo = tipo;
	}

	public Contato() {

	}

	public Contato(String nome, String telefone, Integer anoNasc, Date dataNascimento, TipoContatoEnum tipo) {
		this.nome = nome;
		this.telefone = telefone;
		this.anoNasc = anoNasc;
		this.dataNascimento = dataNascimento;
		this.tipo = tipo;
	}/*
		 * J� criar o objeto com valores com um ou mais dos seus atributos, colocar
		 * regras de n�gocio
		 */

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public Integer getAnoNasc() {
		return anoNasc;
	}

	public void setAnoNasc(Integer anoNasc) {
		this.anoNasc = anoNasc;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
}
