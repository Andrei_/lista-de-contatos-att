package poo.estrutura.base.business;

import java.util.List;

import poo.estrutura.base.domain.Contato;
import poo.estrutura.base.persistence.ContatoDAO;

public class ContatoBO {
	private ContatoDAO contatoDAO = new ContatoDAO();

	public String include(Contato contato) throws Exception {
		validar(contato);
		return contatoDAO.include(contato);
	}

	public List<Contato> getAll() {
		return contatoDAO.getAll();
	}

	public Contato findByNome(String txt) {
		return contatoDAO.findByNome(txt);
	}
    public Contato find(String txt, String telefone) {
    	contatoDAO.find(txt, telefone);
    	if(contatoDAO.find(txt, telefone) == null) {
    		System.out.println("Contato inexistente!!");
    	}
    	return contatoDAO.find(txt, telefone);
    }
	public void validar(Contato contato) throws Exception {
		// while(true){
		// try {
		// contato.getNome().trim().isEmpty();
		// contato.getTelefone().trim().isEmpty();
		// } catch (Exception e) {
		// System.out.println("Voc� deixou o espa�o em branco.");
		// }
		// }
		validarTextoObrigatorio(contato.getNome(), "nome");
		validarTextoObrigatorio(contato.getTelefone(), "telefone");
	}

	public void validarTextoObrigatorio(String texto, String descricao) throws Exception {
		if (texto.trim().isEmpty()) {
			throw new Exception("Voc� deixou o " + descricao + " em branco");
		}
	}
	
}
